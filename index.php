<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

$app->contentType('application/json; charset=utf-8');

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */

// GET route
$app->get(
    '/',
    function () {
        echo 'Root path is here';
    }
);

$app->get('/posts', function() {
    require_once 'lib/mysql.php';
    $db = connect_db();
    $db->query("SET NAMES utf8");
    $db->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
    $result = $db->query('SELECT id, author, title, content, date FROM posts;');
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $data[] = $row;
    }
    header("Content-Type: application/json;charset=UTF-8");
    echo json_encode($data);
});

//$app->post('/create-new-post', function () use ($app) {
//    $data = $app->request->getBody();
//    echo 'Data received: ' + $data;
//});

//$app->get('/login', function () use ($app) {
//    $data = $app->request->getBody();
//    $result = $db->query('SELECT username, password, fullname FROM users WHERE username = '+ $data.username +';');
//
//    if (password is valid)
//        return fullname;
//    echo 'invalid username or password';
//});

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
